package com.bca.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
// import com.google.gson.reflect.TypeToken;

import adapter.PINAdapter;
import model.ErrorStatus;
import adapter.ErrorAdapter;
import adapter.LogAdapter;

@RestController
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String GetPing() {
	String ConnectionStatus = "true";
	return ConnectionStatus;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody String PINActivation(@RequestBody model.mdlCardRegistrationActivation mdlCardRegistrationActivation, HttpServletResponse response) {

	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;

	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	String serialNumber = mdlCardRegistrationActivation.serial_number;
	String wsid = mdlCardRegistrationActivation.wsid;
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.WSID = wsid;
	mdlLog.SerialNumber = serialNumber;
	mdlLog.ApiFunction = "PINActivation";
	mdlLog.SystemFunction = "PINActivation";
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";

	String jsonIn = gson.toJson(mdlCardRegistrationActivation);
	String jsonOut = "";

	try {
	    boolean isValid = true;

	    if (mdlCardRegistrationActivation.account_number == null || mdlCardRegistrationActivation.account_number.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_01);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.card_number == null || mdlCardRegistrationActivation.card_number.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_02);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.user_id == null || mdlCardRegistrationActivation.user_id.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_03);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.transaction_date == null || mdlCardRegistrationActivation.transaction_date.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_04);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.branch_name == null || mdlCardRegistrationActivation.branch_name.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_05);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.terminal_id == null || mdlCardRegistrationActivation.terminal_id.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_06);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.track_data == null || mdlCardRegistrationActivation.track_data.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_07);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.old_pin_block == null || mdlCardRegistrationActivation.old_pin_block.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_08);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.new_pin_block == null || mdlCardRegistrationActivation.new_pin_block.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_09);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.confirm_new_pin_block == null || mdlCardRegistrationActivation.confirm_new_pin_block.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_10);
		isValid = false;
	    } else if (mdlCardRegistrationActivation.supervisor_id == null || mdlCardRegistrationActivation.supervisor_id.equals("")) {
		mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_11);
		isValid = false;
	    }

	    if (!isValid) {
		LogAdapter.InsertLog(mdlLog);
		jsonOut = gson.toJson(mdlAPIResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : PINActivation, method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return gson.toJson(mdlAPIResult);
	    }
	    model.mdlCardInstant mdlCardInstant = gson.fromJson(jsonIn, model.mdlCardInstant.class);

	    model.mdlAPIResult mdlCardInstantResult = PINAdapter.RegisterNewCard(mdlCardInstant, serialNumber, wsid);

	    // get country data
	    if (mdlCardInstantResult == null || !mdlCardInstantResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		if (mdlCardInstantResult == null) {
		    mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_12);
		} else {
		    mdlAPIResult.ErrorSchema = (model.mdlErrorSchema) mdlCardInstantResult.ErrorSchema;
		}
		LogAdapter.InsertLog(mdlLog);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		jsonOut = gson.toJson(mdlAPIResult);
		logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : PINActivation, Function : RegisterNewCard, method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return gson.toJson(mdlAPIResult);
	    }

	    model.mdlPINActivation mdlPINActivation = gson.fromJson(jsonIn, model.mdlPINActivation.class);
	    model.mdlAPIResult mdlPINActivationResult = PINAdapter.ActivatePINCard(mdlPINActivation, serialNumber, wsid);

	    if (mdlPINActivationResult == null || !mdlPINActivationResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		if (mdlPINActivationResult == null) {
		    mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_13);
		} else {
		    mdlAPIResult.ErrorSchema = (model.mdlErrorSchema) mdlPINActivationResult.ErrorSchema;
		}
		LogAdapter.InsertLog(mdlLog);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		jsonOut = gson.toJson(mdlPINActivationResult);
		logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : PINActivation, Function : ActivatePINCard, method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return gson.toJson(mdlAPIResult);
	    }

	    mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00);
	    mdlAPIResult.OutputSchema = mdlPINActivationResult.OutputSchema;
	    jsonOut = gson.toJson(mdlAPIResult);
	    response.setStatus(HttpServletResponse.SC_OK);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS = ResponseTime : " + elapsedTime + ", API : PINActivation, method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);

	} catch (Exception ex) {
	    mdlAPIResult.ErrorSchema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99);
	    mdlLog.ErrorMessage = ex.toString();
	    jsonOut = gson.toJson(mdlAPIResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : PINActivation, method : POST, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut + ", Exception : " + ex.toString(), ex);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}
	LogAdapter.InsertLog(mdlLog);
	return gson.toJson(mdlAPIResult);
    }
}
