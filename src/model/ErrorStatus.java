package model;

public enum ErrorStatus {
    ERR_00, ERR_01, ERR_02, ERR_03, ERR_04, ERR_05, ERR_06, ERR_07, ERR_08, ERR_09, ERR_10, ERR_11, ERR_12, ERR_13, ERR_14, ERR_15, ERR_99
}
