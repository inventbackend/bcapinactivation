package model;

public class mdlCardRegistrationActivation {
    public String account_number;
    public String user_id;
    public String card_number;
    public String transaction_date;
    public String branch_name;
    public String terminal_id;
    public String track_data;
    public String old_pin_block;
    public String new_pin_block;
    public String confirm_new_pin_block;
    public String supervisor_id;
    public String serial_number;
    public String wsid;
}
