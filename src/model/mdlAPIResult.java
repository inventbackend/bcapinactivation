package model;

import com.google.gson.annotations.SerializedName;

public class mdlAPIResult {
    @SerializedName(value = "error_schema", alternate = "ErrorSchema")
    public model.mdlErrorSchema ErrorSchema;
    @SerializedName(value = "output_schema", alternate = "OutputSchema")
    public Object OutputSchema;
}