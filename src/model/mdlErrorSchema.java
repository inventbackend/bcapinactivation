package model;

import com.google.gson.annotations.SerializedName;

public class mdlErrorSchema {
    @SerializedName(value = "error_code", alternate = "ErrorCode")
    public String ErrorCode;
    @SerializedName(value = "error_message", alternate = "ErrorMessage")
    public model.mdlMessage ErrorMessage;
}
