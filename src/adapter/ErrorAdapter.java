package adapter;

import model.ErrorStatus;

public class ErrorAdapter {
    public static model.mdlErrorSchema GetErrorSchema(ErrorStatus error) {
	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	model.mdlMessage mdlMessage = new model.mdlMessage();
	switch (error) {

	case ERR_00:
	    mdlErrorSchema.ErrorCode = "00";
	    mdlMessage.Indonesian = "Sukses";
	    mdlMessage.English = "Success";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_01:
	    mdlErrorSchema.ErrorCode = "01";
	    mdlMessage.Indonesian = "Nomor rekening tidak valid";
	    mdlMessage.English = "Account number is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_02:
	    mdlErrorSchema.ErrorCode = "02";
	    mdlMessage.Indonesian = "Nomor kartu tidak valid";
	    mdlMessage.English = "Card number is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_03:
	    mdlErrorSchema.ErrorCode = "03";
	    mdlMessage.Indonesian = "User ID tidak valid";
	    mdlMessage.English = "User ID is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_04:
	    mdlErrorSchema.ErrorCode = "04";
	    mdlMessage.Indonesian = "Tanggal transaksi tidak valid";
	    mdlMessage.English = "Transaction date is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_05:
	    mdlErrorSchema.ErrorCode = "05";
	    mdlMessage.Indonesian = "Nama cabang tidak valid";
	    mdlMessage.English = "Branch name is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_06:
	    mdlErrorSchema.ErrorCode = "06";
	    mdlMessage.Indonesian = "Terminal ID tidak valid";
	    mdlMessage.English = "Terminal ID is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_07:
	    mdlErrorSchema.ErrorCode = "07";
	    mdlMessage.Indonesian = "PIN block lama tidak valid";
	    mdlMessage.English = "Old PIN block is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_08:
	    mdlErrorSchema.ErrorCode = "08";
	    mdlMessage.Indonesian = "PIN block lama tidak valid";
	    mdlMessage.English = "Old PIN block is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_09:
	    mdlErrorSchema.ErrorCode = "09";
	    mdlMessage.Indonesian = "PIN block baru tidak valid";
	    mdlMessage.English = "New PIN block is not valid";
	    
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_10:
	    mdlErrorSchema.ErrorCode = "10";
	    mdlMessage.Indonesian = "Konfirmasi PIN block baru tidak valid";
	    mdlMessage.English = "Confirm New PIN block is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_11:
	    mdlErrorSchema.ErrorCode = "11";
	    mdlMessage.Indonesian = "Supervisor ID tidak valid";
	    mdlMessage.English = "Supervisor ID is not valid";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_12:
	    mdlErrorSchema.ErrorCode = "12";
	    mdlMessage.Indonesian = "Gagal memanggil service RegisterNewCard";
	    mdlMessage.English = "Service RegisterNewCard call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_13:
	    mdlErrorSchema.ErrorCode = "13";
	    mdlMessage.Indonesian = "Gagal memanggil service ActivatePINCard";
	    mdlMessage.English = "Service ActivatePINCard call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	case ERR_99:
	    mdlErrorSchema.ErrorCode = "99";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = "Service call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	default:
	    mdlErrorSchema.ErrorCode = "99";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = "Service call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    break;
	}
	return mdlErrorSchema;
    }
}
