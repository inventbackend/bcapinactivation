package adapter;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class PINAdapter {
    final static Logger logger = LogManager.getLogger(PINAdapter.class);
    static Gson gson = new Gson();

    static Client client = Client.create();
    model.mdlLog mdlLog = new model.mdlLog();

    public static model.mdlAPIResult RegisterNewCard(model.mdlCardInstant mdlCardInstant, String serialNumber, String wsid) {
	long startTime = System.currentTimeMillis();
	model.mdlAPIResult mdlCardInstantResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "PINActivation";
	mdlLog.SystemFunction = "RegisterNewCard";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = serialNumber;
	mdlLog.WSID = wsid;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";
	String jsonIn = gson.toJson(mdlCardInstant);
	String jsonOut = "";

	String urlAPI = "/debit-cards/v2/instant";
	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    // Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("client-id", decryptedClientID).post(ClientResponse.class, jsonIn);
	    jsonOut = response.getEntity(String.class);
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS. ResponseTime : " + elapsedTime + ", API : PINActivation, method : POST, function : RegisterNewCard, jsonIn:" + jsonIn + ", jsonOut : " + jsonOut);

	    mdlCardInstantResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    if (mdlCardInstantResult == null) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "API output is null";
	    } else if (!mdlCardInstantResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlCardInstantResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlCardInstantResult = null;
	    mdlLog.LogStatus = "Failed";
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    logger.error("FAILED. ResponseTime : " + elapsedTime + ", API : PINActivation, method : POST, function : RegisterNewCard, jsonIn:" + jsonIn + ", Exception : " + ex.toString(), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlCardInstantResult;
    }

    public static model.mdlAPIResult ActivatePINCard(model.mdlPINActivation mdlPINActivation, String serialNumber, String wsid) {
	long startTime = System.currentTimeMillis();
	model.mdlAPIResult mdlPINActivationResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "PINActivation";
	mdlLog.SystemFunction = "ActivatePINCard";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = serialNumber;
	mdlLog.WSID = wsid;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";

	String jsonIn = gson.toJson(mdlPINActivation);
	String jsonOut = "";

	Gson gson = new Gson();
	String urlAPI = "/debit-cards/v2/pin-activation/";

	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    // Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("client-id", decryptedClientID).post(ClientResponse.class, jsonIn);
	    jsonOut = response.getEntity(String.class);
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS. ResponseTime : " + elapsedTime + ", API : PINActivation, method : POST, function : ActivatePINCard, jsonIn:" + jsonIn + ", jsonOut : " + jsonOut);

	    mdlPINActivationResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    if (mdlPINActivationResult == null) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "API output is null";
	    } else if (!mdlPINActivationResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlPINActivationResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlPINActivationResult = null;
	    mdlLog.LogStatus = "Failed";
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    logger.error("FAILED. ResponseTime : " + elapsedTime + ", API : PINActivation, method : POST, function : ActivatePINCard, jsonIn:" + jsonIn + ", Exception : " + ex.toString(), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlPINActivationResult;
    }
}
