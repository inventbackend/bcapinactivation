package database;

import javax.naming.Context;
import javax.naming.InitialContext;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import javax.sql.DataSource;
import oracle.jdbc.OracleConnection;

public class RowSetAdapter {
    final static Logger logger = LogManager.getLogger(RowSetAdapter.class);

    public static OracleConnection getConnectionWL() {
	OracleConnection conn = null;
	DataSource ds = null;
	try {
	    // Get the base naming context from web.xml
	    Context weblogicparam = (Context) new InitialContext().lookup("java:comp/env");
	    // Get a single value from web.xml
	    String datasourceparam = (String) weblogicparam.lookup("param_datasource");

	    Context context = new InitialContext();
	    ds = (DataSource) context.lookup(datasourceparam);
	    conn = (OracleConnection) ds.getConnection();
	} catch (Exception ex) {
	    logger.error("FAILED. API : BCAPINActivation, Function : getConnectionWL, Exception : " + ex.toString(), ex);
	}
	return conn;
    }

}
